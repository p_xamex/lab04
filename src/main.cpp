#include <fstream>
#include <iostream>
#include <string>

using std::cout;
using std::ifstream;
using std::string;
using std::getline;
using std::endl;

struct Stats {
	string codigo;
	string nome;
	int nascimentos[21];
};

int main() {
	string buffer;
	ifstream infile;
	infile.open("../data/Nascimentos_RN.csv");

	while(getline(infile, buffer)) {
		cout << buffer << endl;
	}
}